import * as express from 'express';
import * as cors from 'cors';
import {EndpointsRouter} from "./modules/endpoints"
import { MessageTypesRouter } from "./modules/messagetypes";
import {TokenRouter} from "./modules/tokens"
const app: express.Application = express();
app.use(express.json());
app.use(cors())
app.use("/token",new TokenRouter().router);
app.all('/api/*',TokenRouter.verifyToken,(req:express.Request,res:express.Response,next)=>{
    next();
})
app.use("/api/endpoints",new EndpointsRouter().router);
app.use("/api/messagetypes", new MessageTypesRouter().router);
export {app};