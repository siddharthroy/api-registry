import {Router,Request,Response} from "express";
import * as jwt from "jsonwebtoken"; 
import {AUTH_TOKEN_PROPS} from "../../applicationConstants.json";
export class TokenRouter{
    
    router:Router;
    constructor(){
        this.router = Router();
        this.router.get("/", async(req:Request,res:Response)=>{
            const token = jwt.sign({ id: "payload" }, AUTH_TOKEN_PROPS.TOKEN_SECRET_KEY, {
                expiresIn: 1800
            })
            res.status(200).send(token);
        })
        this.router.get("/verify",TokenRouter.verifyToken,async(req:Request,res:Response,next:Function)=>{
            res.status(200).send();
        })
    }
    static verifyToken(req:Request,res:Response,next:Function){
        const headers = req.headers;
        const token:any = headers["x-access-token"];
        if(!token)
           return res.status(403).send();
        else
        return jwt.verify(token,AUTH_TOKEN_PROPS.TOKEN_SECRET_KEY,(err:Error,decoded:any)=>{
            if(err)
            res.status(500).send(err.message)
            else
            next();
        })
    }
    

}