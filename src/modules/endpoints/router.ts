import {Router,Request,Response} from "express";
import {Database} from "../database/database";
import {DATABASE} from "../../applicationConstants.json";
import {RouterUtils} from "../utils/router-utils";
import {Schema} from "./schema";
import {EndPointModel} from "./model";
import {ObjectID} from "mongodb"
export class EndpointsRouter{
  router:Router;
  constructor(){
      this.router = Router();
      this.router.get("/", async (req:Request,res:Response)=>{
         try{
           const endpoints = await Endpoints.read();
           res.status(200).send(endpoints);
         }catch(e){
           res.status(500).send(`${e.message}-${e.stack}`);
         }
      });
      this.router.post("/",Schema.DATA_SCHEMA_VALIDATIONS,RouterUtils.reportSchemaErrors, async (req:Request,res:Response)=>{
          try{
            const created = await Endpoints.create(req.body);
            res.status(200).send(created)
          }catch(e){
            res.status(500).send(`${e.message}-${e.stack}`);
          }
      });
      this.router.put("/:id",Schema.DATA_SCHEMA_VALIDATIONS,RouterUtils.reportSchemaErrors,async (req:Request,res:Response)=>{
        try{
          const updated = await Endpoints.update(req.params.id,req.body);
          res.status(200).send(updated)
        }catch(e){
          res.status(500).send(`${e.message}-${e.stack}`);
        }
      });
      this.router.delete("/:id", async (req:Request,res:Response)=>{
        try{
          await Endpoints.delete(req.params.id);
          res.status(200).send()
        }catch(e){
          res.status(500).send(`${e.message}-${e.stack}`);
        }
      })
  }
}
class Endpoints{
  static async read(){
    try{
      const endpoints = await new Database().read({
        collection:DATABASE.COLLECTIONS.ENDPOINTS,
        criteria:{},
        projection:{_id:0}
      })
      return endpoints;
    }catch(e){
      throw e
    }
  }
  static async create(endpoint:EndPointModel){
    try{
      const created = await new Database().create({
        collection:DATABASE.COLLECTIONS.ENDPOINTS,
        payload:endpoint
      })
      return created;
    }catch(e){
      throw e
    }
  }
  static async delete(endpointId:string){
    try{
      const created = await new Database().delete({
        collection:DATABASE.COLLECTIONS.ENDPOINTS,
        criteria:{_id: new ObjectID(endpointId)}
      })
      return created;
    }catch(e){
      throw e
    }
  }
  static async update(endpointId:string,endpoint:EndPointModel){
    try{
      const updated = await new Database().update({
        collection:DATABASE.COLLECTIONS.ENDPOINTS,
        criteria:{_id: new ObjectID(endpointId)},
        payload:endpoint
      })
      return updated;
    }catch(e){
      throw e
    }
  }
}