export interface EndPointModel{
    eventType : string,
    event : string,
    partner : string,
    eventGenerator : string,
    direction : string,
    endpoint : string,
    requestSchema : JSON,
    responseSchema :JSON
  }