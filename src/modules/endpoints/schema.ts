import {check,body} from "express-validator";
export class Schema {
public static readonly DATA_SCHEMA_VALIDATIONS = [
    check('eventType').trim().isLength({min:10, max:50}).withMessage('Please enter eventType of Endpoint of minimum length(10) and max length(50).'),
    check('event').trim().isLength({min:5,max:50}).withMessage('Please enter event of Endpoint of minimum length(5) and max length(50).'),
    check('partner').trim().isLength({min:5,max:50}).withMessage('Please enter partner of Endpoint of minimum length(5) and max length(50).'),
    check('eventGenerator').trim().isLength({min:5,max:50}).withMessage('Please enter eventGenerator of Endpoint of minimum length(5) and max length(50).'),
    check('direction').trim().isIn(["Inbound","Outbound"]).withMessage('Please enter direction of Endpoint as either Inbound or Outbound'),
    check('endpoint').trim().isURL({
      protocols: ['http','https'], 
      require_protocol: true, 
      require_host: true, 
      require_valid_protocol: true, 
      allow_underscores: false, 
      allow_trailing_dot: false, 
      allow_protocol_relative_urls: false, 
      disallow_auth: false
      }).withMessage('Please enter endpoint URL of Endpoint.'),
      body('requestSchema').custom(value=>SchemaValidations.checkIfJSON(value)).withMessage("Please give requestSchema as a JSON"),
      body('responseSchema').custom(value=>SchemaValidations.checkIfJSON(value)).withMessage("Please give responseSchema as a JSON")  
  ]
  
}
class SchemaValidations{
    static checkIfJSON(obj:any){
      return obj != null && typeof obj == 'object';
    }
}