import {Router} from "express";
import {Database} from "../database/database";
import {DATABASE} from "../../applicationConstants.json"
export class MessageTypesRouter {
 router:Router = Router();
 constructor(){
     this.router.get("/:id", async(req,res)=>{
         try{
            const documents = await MessageTypes.getAsPerEventType(req.params.id);
            res.status(200).send(documents)
         }catch(e){
             res.status(500).send(`${e.message}-${e.stack}`);
         }
     })
     this.router.post("/:id", async(req,res)=>res.status(405).send());
     this.router.put("/:id", async(req,res)=>res.status(405).send());
     this.router.get("/:event_id/:event_generator_id", async(req,res)=>{
        try{
           const document = await MessageTypes.getAsPerEventIdAndEventGeneratorId(req.params.event_id,req.params.event_generator_id);
           res.status(200).send(document)
        }catch(e){
            res.status(500).send(`${e.message}-${e.stack}`);
        }
    });
    this.router.post("/:event_id/:event_generator_id", async(req,res)=>res.status(405).send());
    this.router.put("/:event_id/:event_generator_id", async(req,res)=>res.status(405).send());
 }
}
class MessageTypes{
    static async getAsPerEventType(eventType:string){
        try{
            const documents = await new Database().read({
                collection:DATABASE.COLLECTIONS.ENDPOINTS,
                criteria : {eventType},
                projection:{_id:0}
            });
            return documents
        }catch(e){
            throw e
        }
    }
    static async getAsPerEventIdAndEventGeneratorId(event:string,eventGenerator:string){
        try{
            const document = await new Database().readOne({
                collection:DATABASE.COLLECTIONS.ENDPOINTS,
                criteria : {event,eventGenerator},
                options:{ projection:{ _id:0 } }
            });
            return document
        }catch(e){
            throw e
        }
    }
}