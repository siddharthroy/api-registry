import * as request from "supertest";
import {app} from "../src/app"

describe("Token test suite", ()=>{
    let token = "";
    beforeAll( async()=>{
        const response = await request(app).get("/token");
        token = response.text;
    })
    test('GET /token/verify should return true if token valid', async done =>{
       const response = await request(app).get("/token/verify").set({"x-access-token":token});
       const {status} = response;
       expect(status).toBe(200);
       done();
    });
    test('GET /token/verify should fail if token not provided', async done=>{
        const response = await request(app).get("/token/verify");
        const {status} = response;
        expect(status).toBe(403);
        done();
     });
    test('GET /token/verify should error out if invalid token provided', async done=>{
        const response = await request(app).get("/token/verify").set({"x-access-token":"token"});
        const {status} = response;
        expect(status).toBe(500);
        done();
    })
})