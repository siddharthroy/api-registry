import * as request from "supertest";
import {app} from "../src/app"

describe("Message Types Test Suite", ()=>{
    let token = "";
    let insertedId = "";
    beforeAll( async()=>{
        const response = await request(app).get("/token");
        token = response.text;
        const postResponse= await request(app).post("/api/endpoints").set({"x-access-token":token}).send({
           "eventType" : "TEST_EVENT_TYPE",
           "event" : "testEvent",
           "partner" : "Testing Partner",
           "eventGenerator" : "testEventGenerator",
           "direction" : "Inbound",
           "endpoint" : "https://endpoint1.ep.org",
           "requestSchema" : {
               "event" : "Some description about event object"
           },
           "responseSchema" : {
               "event" : "Some description about event object"
           }
        });
        insertedId = postResponse.body.insertedId;
    });
    afterAll( async()=>{
      await request(app).delete(`/api/endpoints/${insertedId}`).set({"x-access-token":token});
    });
    test('GET /api/messagetypes/:id should return 200, if token valid',
    async done=>{
       const verification = await request(app).get("/api/messagetypes/TEST_EVENT_TYPE").set({"x-access-token":token});
       const {status} = verification;
       expect(status).toBe(200);
       done();
    });
    test('GET /api/messagetypes/:id should return the endpoint, input schema or response schema for particular event if token valid',
    async done=>{
       const verification = await request(app).get("/api/messagetypes/TEST_EVENT_TYPE").set({"x-access-token":token});
       const {body} = verification;
       expect(body).toStrictEqual([{
           "eventType" : "TEST_EVENT_TYPE",
           "event" : "testEvent",
           "partner" : "Testing Partner",
           "eventGenerator" : "testEventGenerator",
           "direction" : "Inbound",
           "endpoint" : "https://endpoint1.ep.org",
           "requestSchema" : {
               "event" : "Some description about event object"
           },
           "responseSchema" : {
               "event" : "Some description about event object"
           }
    }]);
       done();
    });
    test('GET /api/messagetypes/:id should return 500 if no matching endpoint found, with valid token',
    async done=>{
       const verification = await request(app).get("/api/messagetypes/UNMATCHED_CRITERIA").set({"x-access-token":token});
       const {status} = verification;
       expect(status).toStrictEqual(500);
       done();
    });
    test('GET /api/messagetypes/:id should give 403,if no token',
    async done=>{
       const verification = await request(app).get("/api/messagetypes/TEST_EVENT_TYPE");
       const {status} = verification;
       expect(status).toBe(403);
       done();
    });
    test('GET /api/messagetypes/:id should give 500,if token invalid',
    async done=>{
       const verification = await request(app).get("/api/messagetypes/TEST_EVENT_TYPE").set({"x-access-token":"invalid_token"});
       const {status} = verification;
       expect(status).toBe(500);
       done();
    });
    test('POST /api/messagetypes/:id should return 403 status if no token', async done=>{
        const verification = await request(app).post("/api/messagetypes/TEST_EVENT_TYPE");
        const {status} = verification;
        expect(status).toBe(403);
        done();
    });
    test('POST /api/messagetypes/:id should return 405 status if valid token', async done=>{
        const verification = await request(app).post("/api/messagetypes/TEST_EVENT_TYPE").set({"x-access-token":`${token}`});
        const {status} = verification;
        expect(status).toBe(405);
        done();
    });
    test('POST /api/messagetypes/:id should return 500 status if invalid token', async done=>{
        const verification = await request(app).post("/api/messagetypes/TEST_EVENT_TYPE").set({"x-access-token":`INVALID_TOKEN`});
        const {status} = verification;
        expect(status).toBe(500);
        done();
    });
    test('PUT /api/messagetypes/:id should return 403 status if no token', async done=>{
        const verification = await request(app).put("/api/messagetypes/TEST_EVENT_TYPE");
        const {status} = verification;
        expect(status).toBe(403);
        done();
    });
    test('PUT /api/messagetypes/:id should return 405 status if valid token', async done=>{
        const verification = await request(app).put("/api/messagetypes/TEST_EVENT_TYPE").set({"x-access-token":`${token}`});
        const {status} = verification;
        expect(status).toBe(405);
        done();
    });
    test('PUT /api/messagetypes/:id should return 500 status if invalid token', async done=>{
        const verification = await request(app).put("/api/messagetypes/TEST_EVENT_TYPE").set({"x-access-token":`INVALID_TOKEN`});
        const {status} = verification;
        expect(status).toBe(500);
        done();
    });
    test('GET /api/messagetypes/:event_id/:event_generator_id returns 403, with no token', async done=>{
        const verification = await request(app).get("/api/messagetypes/testEvent/testEventGenerator");
        const {status} = verification;
        expect(status).toBe(403);
        done();
    })
    test('GET /api/messagetypes/:event_id/:event_generator_id returns 500, with an invalid token', async done=>{
        const verification = await request(app).get("/api/messagetypes/testEvent/testEventGenerator").set({"x-access-token":`invalid_token`});
        const {status} = verification;
        expect(status).toBe(500);
        done();
    })
    test('GET /api/messagetypes/:event_id/:event_generator_id returns 200, with valid token', async done=>{
        const verification = await request(app).get("/api/messagetypes/testEvent/testEventGenerator").set({"x-access-token":`${token}`});
        const {status} = verification;
        expect(status).toBe(200);
        done();
    })
    test('GET /api/messagetypes/:event_id/:event_generator_id returns the endpoint, input schema or response schema for particular event and particular event generator,when valid token provided', async done=>{
        const verification = await request(app).get("/api/messagetypes/testEvent/testEventGenerator").set({"x-access-token":`${token}`});
        const {body} = verification;
        expect(body).toStrictEqual({
            "eventType" : "TEST_EVENT_TYPE",
            "event" : "testEvent",
            "partner" : "Testing Partner",
            "eventGenerator" : "testEventGenerator",
            "direction" : "Inbound",
            "endpoint" : "https://endpoint1.ep.org",
            "requestSchema" : {
                "event" : "Some description about event object"
            },
            "responseSchema" : {
                "event" : "Some description about event object"
            }
     });
        done();
    })
    test('GET /api/messagetypes/:event_id/:event_generator_id returns 500 when no endpoints found,with valid token', async done=>{
        const verification = await request(app).get("/api/messagetypes/INVALID/INVALID").set({"x-access-token":`${token}`});
        const {status} = verification;
        expect(status).toBe(500);
        done();
    })
    test('POST /api/messagetypes/:event_id/:event_generator_id should return 403 status, with no token', async done=>{
        const verification = await request(app).post("/api/messagetypes/testEvent/testEventGenerator");
        const {status} = verification;
        expect(status).toBe(403);
        done();
    });
    test('POST /api/messagetypes/:event_id/:event_generator_id should return 405 status, with valid token', async done=>{
        const verification = await request(app).post("/api/messagetypes/testEvent/testEventGenerator").set({"x-access-token":`${token}`});
        const {status} = verification;
        expect(status).toBe(405);
        done();
    });
    test('POST /api/messagetypes/:event_id/:event_generator_id should return 500 status, with invalid token', async done=>{
        const verification = await request(app).post("/api/messagetypes/testEvent/testEventGenerator").set({"x-access-token":`invalid_token`});
        const {status} = verification;
        expect(status).toBe(500);
        done();
    });
    test('PUT /api/messagetypes/:event_id/:event_generator_id should return 403 status with no token', async done=>{
        const verification = await request(app).put("/api/messagetypes/testEvent/testEventGenerator");
        const {status} = verification;
        expect(status).toBe(403);
        done();
    });
    test('PUT /api/messagetypes/:event_id/:event_generator_id should return 405 status with valid token', async done=>{
        const verification = await request(app).put("/api/messagetypes/testEvent/testEventGenerator").set({
            "x-access-token":`${token}`
        });
        const {status} = verification;
        expect(status).toBe(405);
        done();
    });
    test('PUT /api/messagetypes/:event_id/:event_generator_id should return 500 status with valid token', async done=>{
        const verification = await request(app).put("/api/messagetypes/testEvent/testEventGenerator").set({
            "x-access-token":`invalid_token`
        });
        const {status} = verification;
        expect(status).toBe(500);
        done();
    });
});