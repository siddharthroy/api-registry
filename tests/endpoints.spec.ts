import * as request from "supertest";
import {app} from "../src/app"
import {ObjectID} from "mongodb"
describe('Endpoints Test Suite',()=>{
   let token = "";
   let insertedId = "";
   let insertedIdForPostTestCase = "";
    beforeAll( async()=>{
        const response = await request(app).get("/token");
        token = response.text;
        const postResponse= await request(app).post("/api/endpoints").set({"x-access-token":token}).send({
           "eventType" : "TEST_EVENT_TYPE",
           "event" : "testEvent",
           "partner" : "Testing Partner",
           "eventGenerator" : "testEventGenerator",
           "direction" : "Inbound",
           "endpoint" : "https://endpoint1.ep.org",
           "requestSchema" : {
               "event" : "Some description about event object"
           },
           "responseSchema" : {
               "event" : "Some description about event object"
           }
        });
        insertedId = postResponse.body.insertedId;
    });
    afterAll( async()=>{
      await request(app).delete(`/api/endpoints/${insertedId}`).set({"x-access-token":token});
    });
    test('GET /api/endpoints should fail if no token provided',
     async(done)=>{
        const verification = await request(app).get("/api/endpoints");
        const {status} = verification;
        expect(status).toBe(403);
        done();
     });
   test('GET /api/endpoints should give status 200, if valid token provided',
     async(done)=>{
        const verification = await request(app).get("/api/endpoints").set({"x-access-token":token});
        const {status} = verification;
        expect(status).toBe(200);
        done();
     });
     test('GET /api/endpoints should returns the endpoints, input schemas or response schemas for all events, if valid token provided',
     async(done)=>{
        const verification = await request(app).get("/api/endpoints").set({"x-access-token":token});
        const {body} = verification;
        expect(body.length).toBeGreaterThan(0);
        done();
     });
     test('GET /api/endpoints should return status 500, if no ',
     async(done)=>{
        const verification = await request(app).get("/api/endpoints").set({"x-access-token":token});
        const {body} = verification;
        expect(body.length).toBeGreaterThan(0);
        done();
     });
     test('POST /api/endpoints should fail if no token provided',
     async(done)=>{
        const verification = await request(app).post("/api/endpoints");
        const {status} = verification;
        expect(status).toBe(403);
        done();
     });
     test('POST /api/endpoints should not create an endpoint if token valid and schema invalid',
     async(done)=>{
        const verification = await request(app).post("/api/endpoints").set({"x-access-token":token});
        const {status} = verification;
        expect(status).toBe(422);
        done();
     });
     test('POST /api/endpoints should create an endpoint if token valid and  schema valid',
     async(done)=>{
        const verification = await request(app).post("/api/endpoints").set({"x-access-token":token}).send({
            "eventType" : "ECOVADIS_EVENT_TYPE",
            "event" : "ECOVADIS_test",
            "partner" : "Ecovadis",
            "eventGenerator" : "ECOVADIS_test_one",
            "direction" : "Inbound",
            "endpoint" : "http://ecovadis.ep.org",
            "requestSchema" : {
               "event" : "Some description about event object"
            },
            "responseSchema" : {
               "event" : "Some description about event object"
            }
        });
        insertedIdForPostTestCase = verification.body.insertedId;
        const {status} = verification;
        expect(status).toBe(200);
        done();
     });
     test('POST /api/endpoints should not create same endpoint if token valid and schema valid',
     async(done)=>{
        const verification = await request(app).post("/api/endpoints").set({"x-access-token":token}).send({
            "eventType" : "ECOVADIS_EVENT_TYPE",
            "event" : "ECOVADIS_test",
            "partner" : "Ecovadis",
            "eventGenerator" : "ECOVADIS_test_one",
            "direction" : "Inbound",
            "endpoint" : "http://ecovadis.ep.org",
            "requestSchema" : {
               "event" : "Some description about event object"
            },
            "responseSchema" : {
               "event" : "Some description about event object"
            }
        });
        const {status} = verification;
        expect(status).toBe(500);
        done();
     });
     test('PUT /api/endpoints/id should fail if no token provided',
     async(done)=>{
        const verification = await request(app).put(`/api/endpoints/${insertedId}`);
        const {status} = verification;
        expect(status).toBe(403);
        done();
     });
     test('PUT /api/endpoints should not update an endpoint if token valid and schema invalid',
     async(done)=>{
        const verification = await request(app).put(`/api/endpoints/${insertedId}`).set({"x-access-token":token}).send({
         "eventType" : "ECOVADIS_EVENT_TYPE",
         "event" : "test",
         "partner" : "Ecovadis",
         "eventGenerator" : "test",
         "direction" : "Inbound",
         "endpoint" : "http://ecovadis.ep.org",
         "requestSchema" : {
            "event" : "Some description about event object"
         },
         "responseSchema" : {
            "event" : "Some description about event object"
         }
        });
        const {status} = verification;
        expect(status).toBe(422);
        done();
     });
     test('PUT /api/endpoints should update an endpoint if token valid and  schema valid',
     async(done)=>{
        const verification = await request(app).put(`/api/endpoints/${insertedId}`).set({"x-access-token":token}).send({   
         "eventType" : "ECOVADIS_EVENT_TYPE",
         "event" : "test_ecovadis",
         "partner" : "Ecovadis",
         "eventGenerator" : "testGen_ecovadis",
         "direction" : "Outbound",
         "endpoint" : "http://ecovadis.ep.org",
         "requestSchema" : {
            "event" : "Some description about event object"
         },
         "responseSchema" : {
            "event" : "Some description about event object"
         }
        });
        const {status} = verification;
        expect(status).toBe(200);
        done();
     });
     test('PUT /api/endpoints should not update an endpoint  for same data points,if token valid and  schema valid',
     async(done)=>{
        const verification = await request(app).put(`/api/endpoints/${insertedId}`).set({"x-access-token":token}).send({   
         "eventType" : "ECOVADIS_EVENT_TYPE",
         "event" : "test_ecovadis",
         "partner" : "Ecovadis",
         "eventGenerator" : "testGen_ecovadis",
         "direction" : "Outbound",
         "endpoint" : "http://ecovadis.ep.org",
         "requestSchema" : {
            "event" : "Some description about event object"
         },
         "responseSchema" : {
            "event" : "Some description about event object"
         }
        });
        const {status} = verification;
        expect(status).toBe(500);
        done();
     });
     test('DELETE /api/endpoints/id, should return status 403 without token',
     async(done)=>{
        const verification = await request(app).delete("/api/endpoints");
        const status = verification.status;
        expect(status).toBe(403);
        done();
     });
     test('DELETE /api/endpoints/id should delete and return status 200 with token',
     async(done)=>{
        const verification = await request(app).delete(`/api/endpoints/${insertedIdForPostTestCase}`).set({"x-access-token":token});;
        const status = verification.status;
        expect(status).toBe(200);
        done();
     });
     test('DELETE /api/endpoints/id should return status 500 with valid token but no deleted documents',
     async(done)=>{
        const verification = await request(app).delete(`/api/endpoints/INVALID_ID`).set({"x-access-token":token});;
        const status = verification.status;
        expect(status).toBe(500);
        done();
     });
     test('DELETE /api/endpoints/id should not delete any documents,with valid token',
     async(done)=>{
        const verification = await request(app).delete(`/api/endpoints/${new ObjectID()}`).set({"x-access-token":token});;
        const status = verification.status;
        expect(status).toBe(500);
        done();
     });
})